# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: 'Instant-Translate' '2.1'\n"
"Report-Msgid-Bugs-To: nvda-translations@freelists.org\n"
"POT-Creation-Date: 2013-02-09 18:30+0100\n"
"PO-Revision-Date: 2013-03-26 22:26+0900\n"
"Last-Translator: Takuya Nishimoto <nishimotz@gmail.com>\n"
"Language-Team: Japanese <nvdajp@nvda.jp>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Translators: name of the option in the menu.
msgid "Instant Translate Settings..."
msgstr "インスタント翻訳の設定..."

#. Translators: tooltip text for the menu item.
msgid "Select languages to be used for translation."
msgstr "翻訳したい言語を選択してください"

#. Translators: message presented when user presses the shortcut key for translating clipboard text but the clipboard is empty.
msgid "There is no text on the clipboard"
msgstr "クリップボードにテキストがありません"

#. Translators: Message presented when clipboard text (to be translated) is too long (more than a set limit).
#, python-format
msgid ""
"The clipboard contains a large portion of text. It is %s characters long. "
"The limit is 350 characters."
msgstr ""
"クリップボードのテキストが長すぎます。 %s文字あります。最大は350文字です。"

#. Translators: message presented in input help mode, when user presses the shortcut keys for this addon.
msgid ""
"Translates clipboard text from one language to another using Google "
"Translate."
msgstr "クリップボードのテキストの翻訳をGoogle翻訳で行います"

#. Translators: user has pressed the shortcut key for translating selected text, but no text was actually selected.
msgid "no selection"
msgstr "選択なし"

#. Translators: Message presented when selected text (to be translated) is too long (more than a set limit).
#, python-format
msgid ""
"The selection contains a large portion of text. It is %s characters long. "
"The limit is 350 characters."
msgstr ""
"クリップボードのテキストが長すぎます。 %s文字あります。最大は350文字です。"

#. Translators: message presented in input help mode, when user presses the shortcut keys for this addon.
msgid ""
"Translates selected text from one language to another using Google Translate."
msgstr "クリップボードのテキストの翻訳をGoogle翻訳で行います"

#. Translators: Message presented when the given text (from selected or clipboard) cannot be translated.
msgid "Error translating text. See log for details"
msgstr "翻訳のエラーです。詳細はログを確認してください。"

#. Translators: name of the dialog.
msgid "Instant Translate Settings"
msgstr "インスタント翻訳の設定"

#. Translators: Help message for a dialog.
msgid "Select translation source and target language:"
msgstr "翻訳元と翻訳先の言語の選択:"

#. Translators: A setting in instant translate settings dialog.
msgid "Source language:"
msgstr "翻訳元の言語:"

#. Translators: A setting in instant translate settings dialog.
msgid "Target language:"
msgstr "翻訳元の言語:"

#. Translators: An option to automatically detect source language for translation.
msgid "Automatically detect language"
msgstr "言語の自動検出"

#. Translators: The name of a language supported by this add-on.
msgid "Arabic"
msgstr "アラビア語"

#. Translators: The name of a language supported by this add-on.
msgid "Bangla"
msgstr "バングラ語"

#. Translators: The name of a language supported by this add-on.
msgid "Welsh"
msgstr "ウェルシュ語"

#. Translators: The name of a language supported by this add-on.
msgid "Esperanto"
msgstr "エスペラント語"

#. Translators: The name of a language supported by this add-on.
msgid "Gujarati"
msgstr "グジャラート語"

#. Translators: The name of a language supported by this add-on.
msgid "Creole Haiti"
msgstr "ハイチ・クレオール語"

#. Translators: The name of a language supported by this add-on.
msgid "Armenian"
msgstr "アルメニア語"

#. Translators: The name of a language supported by this add-on.
msgid "Latin"
msgstr "ラテン語"

#. Translators: The name of a language supported by this add-on.
msgid "Norwegian"
msgstr "ノルウェー語"

#. Translators: The name of a language supported by this add-on.
msgid "Serbian (Latin)"
msgstr "セルビア語(ラテン文字)"

#. Translators: The name of a language supported by this add-on.
msgid "Swahili"
msgstr "スワヒリ語"

#. Translators: The name of a language supported by this add-on.
msgid "Tagalog"
msgstr "タガログ語"

#. Translators: The name of a language supported by this add-on.
msgid "Yiddish"
msgstr "イディッシュ語"

#. Translators: The name of a language supported by this add-on.
msgid "Chinese (Simplified)"
msgstr "中国語(簡体字)"

#. Translators: The name of a language supported by this add-on.
msgid "Chinese (Traditional)"
msgstr "中国語(繁体字)"

#. Add-on description
#. TRANSLATORS: Summary for this add-on to be shown on installation and add-on informaiton.
msgid ""
"Instant Translate - Translates given text using the Google Translate service."
msgstr "Instant Translate - 選択されたテキストをGoogleのサービスで翻訳します"

#. Add-on description
#. Translators: Long description to be shown for this add-on on installation and add-on information
msgid ""
"This addon translates selected or clipboard text using the Google Translate "
"service and presents it. Press NVDA+Shift+T to translate selected text. "
"Press NVDA+Shift+Y to translate clipboard text."
msgstr ""
"このアドオンは選択されたテキストやクリップボードのテキストをGoogle翻訳で翻訳"
"します。\n"
"NVDA+Shift+Tを押すと選択範囲を翻訳します。\n"
"NVDA+Shift+Yを押すとクリップボードのテキストを翻訳します。"
