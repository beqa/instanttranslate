# Gyorsfordító #

* Készítők: Alexy Sadovoy, ruslan, Beqa Gozalishvili és a többi NVDA
  fejlesztő.
* [2.2 beta2 verzió][1] letöltése

A kiegészítő segítségével egy kijelölt/vágólapra másolt szöveget fordíthat
le egyik nyelvről a másikra, a Google fordító segítségével.

## Nyelvek beállítása ##

A forrás és célnyelv konfigurálásához válassza az NVDA menü -Beállítások
-Gyorsfordító beállításai menüpontot. Itt két kombinált listamezőben
kiválaszthatja a nyelveket.

## Hogyan használható ez a kiegészítő? ##

Kétféle módon használhatja ezt a bővítményt:

1. Jelölje ki a lefordítandó szöveget a kijelölési parancsokkal
   (pl. Shift+nyilak), majd nyomja meg az NVDA+shift+t billentyűparancsot. A
   program hamarosan bemondja a lefordított szöveget.
2. Másoljon a vágólapra egy szöveget, majd nyomja meg az NVDA+shift+y
   billentyűparancsot a szöveg lefordításához.

## A 2.2 verzió változásai ##
* A maximális karakterek számának megnövelése 1500-ra.
* Gyorsbillentyű hozzáadása a Gyorsfordító menüjének eléréséhez.
* Egy jelölőnégyzet hozzáadása a fordítás eredményének konfigurálásához.
* A konfig fájl a beállítások főmappájában található.
* Új nyelvek: Finn, Német, Francia, Galíciai, Kóreai, Magyar, Német, Olasz,
  Spanyoll, Szlovák, Tamil, Török.

## A 2.1 verzió változásai ##
* Mostantól a kiegészítő képes a vágólapon lévő szöveg lefordítására is az
  nvda+shift+y billentyűparanccsal.

## A 2.0 verzió változásai ##
* Grafikus ablak hozzáadása, ahol kiválasztható a forrás és a célnyelv.
* A kiegészítő menüjének hozzáadása a beállítások menühöz.
* A beállítások mostantól egy elkülönített konfig fájlba írodnak.
* A fordítás eredménye automatikusan a vágólapra kerül, így később ez
  könnyen használható.

## Az 1.0 verzió változásai ##
* Első változat

[[!tag dev]]

[1]: http://addons.nvda-project.org/files/get.php?file=it
