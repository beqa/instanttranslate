# instantTranslate #

* Auteurs: Alexy Sadovoy, ruslan, Beqa Gozalishvili en anderen.
* download [versie 2.2beta2][1]

Deze add-on kan geselecteerde tekst of tekst van het klembord vertalen. Dit
wordt gedaan met de Google Translate dienst.

## Talen instellen ##

Om de bron- en doeltaal in te stellen kiest u InstantTranslate uit het NVDA
instellingen menu. Er zijn twee keuzelijsten: vertalen van en vertalen
naar. Maak uw selectie en bevestig met enter of het klikken op OK. 

## Hoe gebruikt u deze add-on ##

Er zijn twee manieren om deze add-on te gebruiken:

1. Selecteer tekst met bijvoorbeeld shift+pijltoetsen. Gebruik NVDA+shift+t
   om te vertalen. De vertaalde tekstzal in de juiste taal worden
   voorgelezen als de huidige synthesizer dit ondersteunt. 
2. Kopieer tekst naar het klembord en druk op NVDA+shift+y om te vertalen.

## Veranderingen voor 2.2 ##
* Het aantal tekens is verhoogd naar 1500.
* Sneltoets t toegevoegd aan het menu-item Instellingen voor Instant
  Translate
* Selectievakje toegevoegd voor het configureren van het kopiëren van
  vertaalresultaten.
* Het configuratiebestand wordt opgeslagen in de root van de
  instellingenmap.
* Nieuwe vertalingen: Aragonees, Arabisch, Braziliaans Portuguees,
  Kroatisch, Nederlands, Fins, Frans, Gallisch, Duits, Hongaars, Italiaans,
  Japans, Koreaans, Nepali, Pools, Slovaaks, Sloveens, Spaans, Tamil, Turks.

## Veranderingen voor 2.1 ##
* De add-on kan nu tekst vertalen van het klembord als u nvda+shift+y drukt.

## Veranderingen voor 2.0 ##
* Gui configurator toegevoegd waar u de bron- en doeltaal kunt kiezen.
* U vindt de add-on nu in het menu onder Instellingen.
* De instellingen worden nu opgeslagen in een apart configuratiebestand.
* De vertaalresultaten komen automatisch op het klembord zodat u er verder
  mee kunt werken.

## Veranderingen voor 1.0 ##
* Eerste versie.

[[!tag dev]]

[1]: http://addons.nvda-project.org/files/get.php?file=it
