# instantTranslate #

* autores: Alexy Sadovoy, ruslan, Beqa Gozalishvili e outros colaboradores
  do NVDA.
* descarga: [versión 2.2beta2][1]

Este complemento utilízase para traducir texto seleccionado e/ou do
portapapeis dunha lingua a outra.  Esto faise utilizando o servizo de Google
Translate.

## Configurando linguas ##

Para configurar a lingua fonte e a de destiño, dende o menú NVDA, vai a
Preferencias, logo vai a Opcións do Instant Translate.  Hai dous cadros
combinados etiquetados "lingua orixinal" e "Lingua de destiño".  Fai a túa
selección de lingua e preme INTRO sobre o botón Aceptar.

## Como utilizar este complemento ##

Hai dúas maneiras de utilizar este complemento:

1. Selecciona algún texto utilizando ordes de selección (shift con teclas do
   cursor, por exemplo). Entón preme Shift+NVDA+T para traducir o texto
   seleccionado. Entón a cadea traducida lerase, sempre que o sintetizador
   que estás a utilizar soporte a lingua de destiño.
2. Copia algún texto ó portapapeis. Entón preme Shift+NVDA+Y para traducir o
   texto do portapapeis á lingua de destiño.

## Cambios para 2.2 ##
* Aumento do número de caracteres a 1500.
* Engadido un atallo de teclado para o elemento de menú Opcións de Instant
  Translate
* Engadida unha casiña de verificación para a configuración do copiado dos
  resultados da traducción.
* Almacenado do ficheiro config na raíz do cartafol de opcións.
* Novas linguas: Alemán, Coreano, Eslovaco, Español, Francés, Finlandés,
  Galego, Húngaro, Italiano, Tamil, Turco.

## Cambios para 2.1 ##
* Agora o complemento pode traducir texto dende o portapapeis ó premer NVDA
  + shift + y.

## Cambios para  2.0 ##
* Engadido un gui para o configurador onde podes escoller linguas fonte e
  destiño.
* Engadido un elemento de menú ó complemento  que se atopa baixo o menú de
  preferencias.
* As opcións agora escríbense nun ficheiro config separado.
* Os resultados da traducción agora cópianse automáticamente no Portapapeis
  para futuras manipulacións.

## Cambios para  1.0 ##
* Versión inicial.

[[!tag dev]]

[1]: http://addons.nvda-project.org/files/get.php?file=it
